import { Request, Response, Router } from "express";

const routerUser: Router = Router();

/* GET api listing. */
routerUser.get("/", (req: Request, res: Response) => {
  res.send("api works");
});

// Get all posts
routerUser.get("/profile", (req: Request, res: Response) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  const posts = [
    {
      age: 29,
      name: "Long",
      title: "bo",
    },
    {
      age: 29,
      name: "Anh",
      title: "me",
    },
    {
      age: 4,
      name: "Cua",
      title: "con trai",
    },
  ];
  res.status(200).json(posts);
});

export { routerUser };
